#!/bin/sh
# version 1 of manjaro housekeeping bash script
clear
echo ""
echo ""
echo "This Script will REMOVE un-WANTED or un-USED Files on a Manjaro System"
read -p "Press ENTER to continue or Ctrl+C to cancel."

# Vacuum journals
sudo journalctl --vacuum-size=500M && sudo journalctl --vacuum-time=7d

# Remove all uninstalled packages
sudo paccache -rvuk0

# Remove old installed packages, leave 3
sudo paccache -rvk3

# Clean yay cache
yay -Sc -a

# Pacman

# Entfernet alte Pakete aus dem Puffer sowie ungenutzte Repositorien
sudo pacman -Sc
# keine Infos dazu
sudo pacman -Qdt

#Pamac

# Clean pamac build cache
pamac clean -bv
#Remove orphaned libraries
pamac remove --orphans
#clean cache
pamac clean --build-files

# Clean temporary build files
rm -rf ~/{.cargo,.cmake,.electron,.electron-gyp,.npm,.nvm,.racket,.stack,.yarn} || true
rm -rf ~/.cache/{electron,electron-builder,go-build,node-gyp,pip,yarn} || true
sudo rm -rf ~/go || true

#Cache

# delete files in ~/.cache directory that are have not been accessed in 100 days
find ~/.cache -depth -type f -mtime +100 -delete
