## Why ZSH?

ZSH is an amazing shell that just makes everything a bit easier from auto suggestions, completing tasks you do regularly considerably faster.

## Dependencies

Packages needed before you start:

Package|Description
---|---
zsh|ZSH Shell
yay|AUR Helper

## Installation

1. Step: Install oh-my-zsh Extension for ZSH Shell

    `sh -c "$(wget https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh -O -)"`

2. Step: Install recommended plugins

- zsh-syntax-highlighting - syntax highlighting for ZSH in standard repos
- zsh-autosuggestions - Suggestions based on your history
- history-substring-search - type in any part of any command from history and then press chosen keys, such as the UP and DOWN arrows, to cycle through matches.
- autojump - jump to directories with j or jc for child or jo to open in file manager

On Arch/Manjaro/EndeavourOS/Arco:
    `sudo pacman -S zsh-syntax-highlighting zsh-autosuggestions && yay autojump`

On Debian/Ubuntu/MX Linux/Siduction:
    `sudo apt install zsh-syntax-highlighting zsh-autosuggestions autojump`

## Get my .conf files:

- Get my ZSH-config (Manjaro Linux only)

    `wget https://gitlab.com/ProfP303/dotconf/-/raw/main/.zshrc -O ~/.zshrc`

- Get my aliases (Manjaro and Arco Linux)

    `wget https://gitlab.com/ProfP303/dotconf/-/raw/main/.oh-my-zsh/custom/my_zsh_aliases.zsh -O ~/.oh-my-zsh/custom/my_zsh_aliases.zsh`

## Alternative for BASH

1. Step: Install powerliner-shell via PIP

    `pip install powerline-shell`

2. Step Activate powerline-shell in the `~.bashrc` file (attach to the end)

    ```
    function _update_ps1() {
        PS1=$(powerline-shell $?)
    }


    if [[ $TERM != linux && ! $PROMPT_COMMAND =~ _update_ps1 ]]; then

        PROMPT_COMMAND="_update_ps1; $PROMPT_COMMAND"

    fi
    ```
