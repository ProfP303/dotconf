VideoContext Menü erstellen
===========================

## Schritt 1: VideoContextMenu herunterladen und temporär auspacken
https://github.com/kachurovskiy/VideoContextMenu/archive/refs/heads/master.zip

## Schritt 2: Ordner erstellen
%USERPROFILE%\Documents\VideoContextMenu

## Schritt 3: FFmpeg-Build runterladen und installieren
https://github.com/BtbN/FFmpeg-Builds/releases/download/autobuild-2021-07-07-12-37/ffmpeg-n4.4-78-g031c0cb0b4-win64-gpl-4.4.zip

Alle \bin\*.exe ablegen z.B. nach
%LOCALAPPDATA%\ffmpeg

Diesern Ordner zum User Environment Path hinzufügen

## Schritt 4: Registry-Script ausführen
Aus dem temporären Ordner in Schritt 1 die Datei "reg.all" anpassen mit den Pfaden aus Schritt 2, bsp.:
    
    Windows Registry Editor Version 5.00

    [HKEY_CLASSES_ROOT\*\shell\Video: stabilize\command]
    @="%USERPROFILE%\\Documents\\VideoContextMenu\\stabilize.bat \"%1\""

    [HKEY_CLASSES_ROOT\*\shell\Video: to h264\command]
    @="%USERPROFILE%\\Documents\VideoContextMenu\\convert.bat \"%1\""

    
"reg.all" mit erhöhten rechten ausführen
