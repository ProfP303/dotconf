#!/usr/bin/env python3
"""
Project: python scripting tools
Author: Paul Salajean
Version: 0.1
File: librewolf_enable_opengl.py
Summary: Enables OpenGL settings in LibreWolf
Launch: sudo python3 librewolf_enable_opengl.py
"""

# local globals
LIBREWOLF_CONFIG_FILE = "/usr/lib/librewolf/librewolf.cfg"

file = open(LIBREWOLF_CONFIG_FILE,'r')
Lines_to_write = []
Lines_read = file.readlines()

line_nr = 0
for line in Lines_read:
    line_nr += 1
    if 'defaultPref("webgl.disabled"' in line \
    or 'lockPref("webgl.enable-webgl2",' in line \
    or 'lockPref("webgl.min_capability_mode",' in line \
    or 'lockPref("pdfjs.enableWebGL"' in line \
    or 'lockPref("webgl.disable-extensions"' in line \
    or 'lockPref("webgl.disable-fail-if-major-performance-caveat",' in line \
    or 'lockPref("webgl.enable-debug-renderer-info"' in line:
        line = "//# " + line
        print(f"- changed line: #{line_nr} {line}")

    Lines_to_write.append(line)

file.close()

# Writing to file
with open(LIBREWOLF_CONFIG_FILE, "w") as file:
    file.writelines(Lines_to_write)

print("Done.")
